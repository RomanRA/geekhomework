package lesson_1.Marathon.helpers;

import lesson_1.Marathon.obstractor.Obstacle;

public class ObstracleCreate {
// = {new Cross(80), new Wall(2), new Wall(1), new Cross(120)}
    private Obstacle[] course;

    public Obstacle[] getCourse() {
        return course;
    }

    public void setCourse(Obstacle... course) {
        this.course = course;
    }
}
