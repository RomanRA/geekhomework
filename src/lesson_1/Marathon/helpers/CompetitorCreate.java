package lesson_1.Marathon.helpers;

import lesson_1.Marathon.competitors.Competitor;

public class CompetitorCreate {

    private Competitor[] competitors;

    public Competitor[] getCompetitors() {
        return competitors;
    }

    public void setCompetitors(Competitor... player) {
        this.competitors = player;
    }
}
