package lesson_1.Marathon;

import lesson_1.Marathon.competitors.Cat;
import lesson_1.Marathon.competitors.Dog;
import lesson_1.Marathon.competitors.Human;
import lesson_1.Marathon.helpers.CompetitorCreate;
import lesson_1.Marathon.helpers.ObstracleCreate;
import lesson_1.Marathon.helpers.СompetitionCreate;
import lesson_1.Marathon.obstractor.Cross;
import lesson_1.Marathon.obstractor.Wall;
import lesson_1.Marathon.obstractor.Water;

public class Main {


    public static void main(String[] args) {
        CompetitorCreate competitorCreate = new CompetitorCreate();
        ObstracleCreate obstracleCreate = new ObstracleCreate();

        competitorCreate.setCompetitors(new Human("Bob"), new Cat("Kitty"), new Dog("Guff"));
        obstracleCreate.setCourse(new Cross(80), new Wall(2), new Wall(1), new Cross(120), new Water(25));

        new СompetitionCreate(competitorCreate.getCompetitors(), obstracleCreate.getCourse());
    }
}